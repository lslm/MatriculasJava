package matriculas;

import java.util.List;
import java.util.ArrayList;

public class Curso {
    private String nome;
    private double valor = 10000;
    private int numeroInteressados;
    private List<String> alunos = new ArrayList<String>();
    private boolean ofereceCurso = false;
    
    public void setNome(String n) {
        nome = n;
    }
    
    public String getNome() {
        return nome;
    }
    
    public void AdcionarAluno(String nome) throws Exception {
        
        if (alunos.contains(nome.toUpperCase())){
            throw new Exception("Aluno já cadastrado");
        }
        
        alunos.add(nome.toUpperCase());
        
        if (alunos.size() > numeroInteressados) {
            ofereceCurso = true;
        } else {
            ofereceCurso = false;
        }
    }
    
    public void setNumeroInteressados(int n){
        numeroInteressados = n;
    }
    
    public int getNumeroInteressados() {
        return numeroInteressados;
    }
    
    public boolean getOfereceCurso() {
        return ofereceCurso;
    }
    
    public double getValor() {
        return valor / alunos.size();
    }
    
    public int getTotalAlunos() {
        return alunos.size();
    }
}
