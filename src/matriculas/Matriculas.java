package matriculas;
import java.util.Scanner;

public class Matriculas {

    public static void main(String[] args) {
        
        boolean continua = true;
        String resposta;
        Curso curso = new Curso();
        Scanner entrada = new Scanner(System.in);
        
        System.out.print("Digite o nome do curso: ");
        curso.setNome(entrada.next());
        
        System.out.print("Digite a quantidade mínima de alunos interessados: ");
        curso.setNumeroInteressados(entrada.nextInt());
        
        
        do{
            try{
                System.out.print("Digite o nome do aluno: ");
                curso.AdcionarAluno(entrada.next());
                System.out.print("Continuar? (s/n): ");
                resposta = entrada.next();
                continua = (resposta.equals("s")) ? true : false;    
            } catch(Exception e) {
                System.out.println(e.getMessage());
            }
            
        } while (continua == true);
        
        System.out.println("Valor do curso: " + curso.getValor() + 
                "\nTotal de alunos: " + curso.getTotalAlunos() + 
                "\nQuantidade mínima de alunos interessados: " + curso.getNumeroInteressados() + 
                "\nCurso será oferecido: " + curso.getOfereceCurso());
    }
    
}
